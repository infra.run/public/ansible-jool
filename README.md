# ansible-jool

Ansible role for Jool NAT64 gateway

defaults:
```
jool__nat64_prefix: "64:ff9b::/96"
```

Keep in mind to:
- disable gro on the hosts (hypervisors) interfaces
- allow fordwarding in firewall
- enable ipv4 and ipv6 forwarding in sysctl
